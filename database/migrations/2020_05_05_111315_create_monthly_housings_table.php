<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthlyHousingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_housings', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('area', 150);
            $table->integer('average_price');
            $table->string('code', '50');
            $table->integer('houses_sold')->nullable(true);
            $table->decimal('no_of_crimes', 10, 1)->nullable(true);
            $table->boolean('borough_flag')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_housings');
    }
}
