<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class UploadTest extends TestCase
{
    /** @test */
    function upload_file_test()
    {
        $header = 'date,area,average_price,code,houses_sold,no_of_crimes,borough_flag';
        $row1 = '2010-02-01,london,278753,E12000007,6312,,0';
        $row2 = '2011-02-01,london,287983,E12000007,5870,1,0';
        $row3 = '2012-12-01,london,313744,E12000007,7601,,0';

        $content = implode("\n", [$header, $row1, $row2, $row3]);

        $inputs = [
            'csv_file' =>
                UploadedFile::
                fake()->
                createWithContent(
                    'test.csv',
                    $content
                )
        ];

        $response = $this->postJson(
            '/api/upload',
            $inputs
        );

        $response->assertOk();
    }

    /** @test */
    function upload_invalid_csv_test()
    {
        $header = 'date,area,average_price,code,houses_sold,no_of_crimes,borough_flag, test';
        $row1 = '2010-02-01,london,278753,E12000007,6312,,0';
        $row2 = '2011-01-01,london,287983,E12000007,5870,,0';
        $row3 = '2012-12-01,london,313744,E12000007,7601,,0';

        $content = implode("\n", [$header, $row1, $row2, $row3]);

        $inputs = [
            'csv_file' =>
                UploadedFile::
                fake()->
                createWithContent(
                    'test.csv',
                    $content
                )
        ];

        $response = $this->postJson(
            '/api/upload',
            $inputs
        );

        $json["errors"]["general"][] = "Invalid CSV structure";

        $response->assertExactJson($json);
    }
}
