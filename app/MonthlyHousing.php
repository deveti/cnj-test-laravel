<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyHousing extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'date', 'area', 'average_price', 'code', 'houses_sold', 'no_of_crimes', 'borough_flag'
    ];
}
