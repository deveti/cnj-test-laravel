<?php

namespace App\Services;

use App\MonthlyHousing;
use DateTime;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploaderService {

    /** @var string $uploadFolder */
    protected $uploadFolder = '/uploads/csv';

    /** @var string uploadedFilePath */
    protected $uploadedFilePath = null;

    /** @var array $rowsCollection */
    protected $rowsCollection = null;

    /** @var array $columns */
    protected $columns = [
        'date', 'area', 'average_price', 'code', 'houses_sold', 'no_of_crimes', 'borough_flag',
    ];

    /** @var array $summary */
    protected $summary = [
        'avg_price' => 0,
        'count_sold_houses' => 0,
        'num_crimes_2011' => 0,
        'avg_price_london' => 0,
    ];

    /**
     * @param UploadedFile $file
     * @param bool $saveToDb
     * @return array
     * @throws Exception
     */
    public function process(UploadedFile $file, $saveToDb = false)
    {
        $this->upload($file);
        if(!$this->uploadedFilePath) {
            throw new Exception('Error uploading file.');
        }

        $this->loadCsv();
        if($this->rowsCollection->isEmpty()) {
            throw new Exception("Error parsing CSV");
        }

        if($saveToDb) {
            $this->saveToDatabase();
        }

        $this->parseSummary();

        return $this->summary;
    }

    /**
     * Upload file to uploads folder.
     *
     * @param UploadedFile $uploadedFile
     * @param string|null $name
     * @param string|null $folder
     * @param string $disk
     * @return false|string
     */
    private function upload(UploadedFile $uploadedFile, $name = null, $folder = null, $disk = 'public')
    {
        $fileName   = $name ?: Str::slug(time(). '_' . Str::random(32));
        $fileFolder = $folder ?: $this->uploadFolder;

        $this->uploadedFilePath = $uploadedFile->storeAs($fileFolder, $fileName.'.'.$uploadedFile->getClientOriginalExtension(), $disk);
    }

    /**
     * Load uploaded CSV and prepare rows collection.
     */
    private function loadCsv()
    {
        $csvFile = Storage::disk('public')->path($this->uploadedFilePath);

        $this->rowsCollection = collect();
        $rows = 0;

        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 10000, ",")) !== FALSE) {

                // skip first row
                if($rows == 0) {
                    $this->checkIfValidCsv($row);
                    $rows++;
                    continue;
                }

                // map column keys with our data and add to rows array
                $data = array_combine($this->columns, $row);
                $data['houses_sold'] = empty($data['houses_sold']) ? 0 : $data['houses_sold'];
                $data['no_of_crimes'] = empty($data['no_of_crimes']) ? 0 : $data['no_of_crimes'];
                $data['borough_flag'] = empty($data['borough_flag']) ? 0 : $data['borough_flag'];

                $this->rowsCollection->push($data);
                $rows++;

            }
            fclose($handle);
        }
    }

    private function checkIfValidCsv($headers)
    {
        if ($headers !== $this->columns) {
            throw new Exception("Invalid CSV structure");
        }
    }

    /**
     * Parse summary fields for display after upload.
     */
    private function parseSummary()
    {
        // average of all prices sold
        $this->summary['avg_price'] = $this->rowsCollection->avg('average_price');

        // count of all houses sold
        $this->summary['count_sold_houses'] = $this->rowsCollection->sum('houses_sold');

        // number of crimes in 2011 (by the way date data is structured, 01-01 of every year is data for last month, so 12th month)
        $this->summary['num_crimes_2011'] = $this->rowsCollection->whereBetween('date', ['2011-02-01', '2012-01-01'])->sum('no_of_crimes');

        // average price per year in the London area
        $avgPriceLondon = $this->rowsCollection->where('area', 'london')->groupBy(function($item) {
            $date = DateTime::createFromFormat('Y-m-d', $item['date']);
            return $date->format('Y');
        })->map(function($collection, $year) {
            return $collection->avg('average_price');
        })->toArray();

        $this->summary['avg_price_london'] = $avgPriceLondon;
    }

    /**
     * Insert CSV rows (by chunks of 1000 to reduce SQL queries) to DB.
     *
     * @throws Exception
     */
    private function saveToDatabase()
    {
        try {
            $this->rowsCollection
                ->chunk(1000)
                ->each(function($rows) {
                    $data = [];
                    foreach ($rows as $row) {
                        $data[] = $row;
                    }

                    MonthlyHousing::insert($data);
                });

        } catch (Exception $exception) {
            throw new Exception("Error inserting CSV records to database");
        }
    }
}
