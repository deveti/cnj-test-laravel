<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormUploadRequest;
use App\Services\UploaderService;
use Illuminate\Http\JsonResponse;

class UploaderController extends Controller
{
    protected $uploaderService;

    public function __construct(UploaderService $uploaderService)
    {
        $this->uploaderService = $uploaderService;
    }

    /**
     * Store submitted form.
     *
     * @param FormUploadRequest $request
     * @return JsonResponse
     */
    public function store(FormUploadRequest $request)
    {
        try {
            $summary = $this->uploaderService->process($request->csv_file, (bool) $request->save_to_db);
            return response()->json(["data" => $summary], 200);
        } catch (\Exception $e) {
            $msg["errors"]["general"][] = $e->getMessage();
            return response()->json($msg, 500);
        }
    }
}
