# CNJ Digital - Laravel Test

I prepared docker image you can use to run this Laravel app.
Repo: https://gitlab.com/deveti/cnj-test-docker

# Overview

I created simple controller with one method to handle upload and parsing of CSV. Controller calls my service which has one exposed method.

Logic inside the service could be broken apart a little more, but since this was not really complex tax I decided to keep it simple. I was thinking about dispatching a job for data insert, but I decided not to, because it adds another complexity which I think is not needed.
I used Laravel collections to calculate summary fields. Higher order function ftw! :)

In JS resources I created VueJS component for handling simple form validation and file upload. In real life application I would probably use some sort of validation library.

There are 2 tests (in real life there would be few more) one to test success and one to test failure when wrong file is being uploaded.

In real life application there would probably also be some error logging (in more details), I choose to throw some exceptions where I saw fit.

# Installation

1. git clone git@gitlab.com:deveti/cnj-test-laravel.git
2. composer install
3. cp .env.example .env
3. Fill .env file with DB settings
4. php artisan key:generate
5. php artisan storage:link
6. php artisan migrate
7. php artisan test (to run tests)
