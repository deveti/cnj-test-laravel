<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/upload', 'UploaderController@store');

/*
 * Default, catch-all route.
 */
Route::fallback(function(){
    return response()->json([
        'message' => 'Endpoint Not Found. If error persists, contact administrator'
    ], 404);
});
