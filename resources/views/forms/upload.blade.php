@extends('layouts.app')

@section('content')

    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="/img/cnj-logo.png" alt="" width="72" height="72">
        <h2>CNJ Digital - Laravel Test</h2>
        <p class="lead">This test is a sample of the day to day requirements at CNJ Digital.</p>
    </div>

    <uploader-component/>
@endsection
